#include <iostream>
#include "DefinitionMacros.h"

class Object
{
	PRIVATE(char*,GET_SET,m_char,CharTest);
	PRIVATE(float,GET,m_float,FloatTest);
	PRIVATE(int,NONE,m_int,NULL)
};

int main() {

	Object obj;

	obj.SetCharTest("test");
	std::cout << obj.GetCharTest() << std::endl;
	obj.SetCharTest("test2");
	std::cout << obj.GetCharTest() << std::endl;

	//ERROR
	//obj.SetFloatTest(5.0f);

	//ERROR
	//obj.m_char;

	//ERROR
	//std::cout << obj.m_int << std::endl;

	std::cout << obj.GetFloatTest() << std::endl;
}

